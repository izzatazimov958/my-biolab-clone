import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const api = 'https://phoenix-global-tradings-server.com/partners'

export default new Vuex.Store({
	state: {
		partners: {},
	},
	mutations: {
		setPartnersList(state, data) {
			state.partners = data
		}
	},
	actions: {
		async getPartners(state) {
			const data = await axios.get()
			state.commit('setPartnersList', data.data)
		}
	},
	getters: {
		partners_list: (state) => state.partners
	},
})